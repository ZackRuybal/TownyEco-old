package com.writedive.townyeco.messaging;

import com.writedive.townyeco.TownyEco;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TownyEcoLogger {
    // Init Plugin
    private static Plugin plugin = TownyEco.getPlugin(TownyEco.class);

    // Get log path
    private static String getRootFolder() {
        return plugin.getDataFolder().getPath();
    }
    private static String PATH = getRootFolder() + File.separator + "logs";


    public static void log(String text, LogType logType) {
        // Initialize Buffered Writer and File Writer
        BufferedWriter bw = null;
        FileWriter fw = null;

        // Define the date formatting
        SimpleDateFormat sdf = new SimpleDateFormat("[MM/dd/YYYY] [hh:mm:ss a]");
        Date date = new Date();
        try {
            // Test if file exists, if not, create file
            File file = new File(PATH, logType.getName());
            if(!file.exists()) {
                file.createNewFile();
            }

            fw = new FileWriter(file, true);
            bw = new BufferedWriter(fw);

            bw.write(sdf.format(date) + " " + text + System.getProperty("line.separator"));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if(fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    // Define log types, making it really easy to add log files in the future
    public enum LogType {
        STARTUP("startup.log"),
        ECONOMY("economy.log"),
        DATABASE("database.log");
        private String name;
        LogType(String name) {this.name = name; }
        public String getName(){ return name; }
    }

}
