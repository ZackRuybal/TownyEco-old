package com.writedive.townyeco;

import com.writedive.townyeco.commands.MoneyCommand;
import com.writedive.townyeco.configuration.ConfigurationHandler;
import com.writedive.townyeco.db.DataSource;
import com.writedive.townyeco.db.FlatFileDataSource;
import com.writedive.townyeco.db.SQLDataSource;
import com.writedive.townyeco.economy.EconomyManager;
import com.writedive.townyeco.economy.VaultEconomy;
import com.writedive.townyeco.listener.PlayerListener;
import com.writedive.townyeco.messaging.TownyEcoLogger;
import com.writedive.townyeco.objects.TENpc;
import com.writedive.townyeco.objects.TEPlayer;
import com.writedive.townyeco.tasks.PlayerLoginTask;
import com.writedive.townyeco.utilities.BukkitTools;
import com.writedive.util.FileMgmt;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Hashtable;
import java.util.UUID;

import static com.writedive.townyeco.messaging.TownyEcoLogger.LogType.DATABASE;
import static com.writedive.townyeco.messaging.TownyEcoLogger.LogType.ECONOMY;
import static com.writedive.townyeco.messaging.TownyEcoLogger.LogType.STARTUP;

public final class TownyEco extends JavaPlugin {
	private static Economy vaultEconomy = null;
	private PlayerListener playerListener = new PlayerListener(this);
	private Plugin plugin;
	private String version;
	private EconomyManager economy;
	private static DataSource dataSource;
	protected Hashtable<UUID, TEPlayer> players = new Hashtable<>();
	protected Hashtable<UUID, TENpc> npcs = new Hashtable<>();

	public TownyEco() {
		plugin = this;
	}

	// ################################
	// #            ENABLE            #
	// ################################
	@Override
	public void onEnable() {
		// Get Version
		version = this.getDescription().getVersion();
		// Initialization of static classes comes now:
		BukkitTools.initialize(this);

		// Load Configuration
		if (loadSettings()) {
			getCommand("moneyy").setExecutor(new MoneyCommand(this));
		} else {

			//This can obviously be replaced with something more specific later.
			TownyEcoLogger.log("Something went wrong", STARTUP);
		}

		// Confirm plugin is loaded
		getLogger().info("TownyEco Successfully Loaded.");
		TownyEcoLogger.log("Plugin Loaded", STARTUP);


		registerEvents();
		if (loadEconomy()) {

		} else {

		}
		// Re login anyone online. (In case of plugin reloading)
		for (Player player : com.palmergames.bukkit.util.BukkitTools.getOnlinePlayers()) {
			if (player != null) {
				if (BukkitTools.scheduleSyncDelayedTask(new PlayerLoginTask(player), 0L) == -1) {
					TownyEcoLogger.log("Loading Error: Player could not be loaded on reload", STARTUP);
				}
			}
		}
	}

	public boolean loadSettings() {
		FileMgmt.checkFolders(new String[]{
				getRootFolder(),
				getRootFolder() + FileMgmt.fileSeparator() + "settings",
				getRootFolder() + FileMgmt.fileSeparator() + "logs"}); // Setup the logs folder here as the logger will not yet be enabled.

		ConfigurationHandler.loadConfig(getRootFolder() + FileMgmt.fileSeparator() + "settings" + FileMgmt.fileSeparator() + "config.yml", getVersion());

		String save = ConfigurationHandler.getSaveDatabase(), load = ConfigurationHandler.getLoadDatabase();

		TownyEcoLogger.log("Database: [Load] " + load + " [Save] " + save, TownyEcoLogger.LogType.DATABASE);

		players.clear();
		npcs.clear();

		if (!loadDatabase(load)) {
			TownyEcoLogger.log("Failed to load database", DATABASE);
			return false;
		}

		try {
			getDataSource().clearBackups();
			// Set the new class for saving.
			setDataSource(save);
			getDataSource().initialize(this);
			getDataSource().backup();

			if (!load.equalsIgnoreCase(save)) {
				getDataSource().saveAll();
			}
		} catch (UnsupportedOperationException e) {
			TownyEcoLogger.log("We do not support database-type: " + save + "!", DATABASE);
			return false;
		}
		return true;

	}

	public void setDataSource(String databaseType) throws UnsupportedOperationException {

		if (databaseType.equalsIgnoreCase("flatfile")) {
			setDataSource(new FlatFileDataSource());
		} else if ((databaseType.equalsIgnoreCase("mysql")) || (databaseType.equalsIgnoreCase("sqlite")) || (databaseType.equalsIgnoreCase("h2"))) {
			setDataSource(new SQLDataSource(databaseType));
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public void setDataSource(DataSource dataSource) {

		TownyEco.dataSource = dataSource;
	}


	public boolean loadDatabase(String databaseType) {

		try {
			setDataSource(databaseType);
		} catch (UnsupportedOperationException e) {
			return false;
		}
		getDataSource().initialize(this);

		return getDataSource().loadAll();
	}

	private void registerEvents() {
		PluginManager pm = getServer().getPluginManager();
		// These should always be loaded!
		pm.registerEvents(playerListener, this);
	}

	private boolean loadEconomy() {
		economy = new EconomyManager(this);
		if (getServer().getPluginManager().getPlugin("Vault") != null) {
			ServicesManager sm = getServer().getServicesManager();
			sm.register(Economy.class, new VaultEconomy(this), this, ServicePriority.Highest);
			RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
			if (!(rsp == null)) {
				vaultEconomy = rsp.getProvider();
			}
			TownyEcoLogger.log("Vault Hooked", ECONOMY);
		}
		// TODO: Add Reserve Support!
		economy.setEnabled(true);
		return true;
	}

	// #################################
	// #            DISABLE            #
	// #################################
	@Override
	public void onDisable() {

		getDataSource().cancelTask();
		TownyEcoLogger.log("Plugin Disabled", STARTUP);
	}
	// #################################
	// #            GETTERS            #
	// #################################

	public Plugin getPlugin() {
		return plugin;
	}

	public String getVersion() {
		return version;
	}

	public EconomyManager getEconomy() {
		return economy;
	}

	public static DataSource getDataSource() {

		return dataSource;
	}

	public Hashtable<UUID, TENpc> getNPCMap() {
		return npcs;
	}

	public Hashtable<UUID, TEPlayer> getPlayerMap() {
		return players;
	}

	public String getRootFolder() {
		return plugin.getDataFolder().getPath();
	}

	public static Economy getVaultEconomy() {
		return vaultEconomy;
	}
}
