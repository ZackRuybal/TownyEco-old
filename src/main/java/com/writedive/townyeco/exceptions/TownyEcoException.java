package com.writedive.townyeco.exceptions;

public class TownyEcoException extends Exception {

	private static final long serialVersionUID = 5857335825616925573L;

	public TownyEcoException() {

		super("No Message");
	}

	public TownyEcoException(String message) {

		super(message);
	}
}
