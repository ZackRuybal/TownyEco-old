package com.writedive.townyeco.exceptions;

public class AlreadyExistingException extends TownyEcoException {

	private static final long serialVersionUID = -5979067912945219471L;

	public AlreadyExistingException() {

		super("Already exists.");
	}

	public AlreadyExistingException(String message) {

		super(message);
	}
}
