package com.writedive.townyeco.tasks;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.exceptions.AlreadyExistingException;
import com.writedive.townyeco.objects.TENpc;

import java.util.UUID;

public class NPCRegisterTask implements Runnable {
	private UUID uuid;

	public NPCRegisterTask(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public void run() {

		TENpc npc;

		if (!TownyEco.getDataSource().hasNPC(uuid)) { // Not possible in theory but still
			try {
				TownyEco.getDataSource().newNPC(uuid);
				npc = TownyEco.getDataSource().getNPC(uuid);
				npc.setRegistered(System.currentTimeMillis());

				TownyEco.getDataSource().saveNPC(npc);
				TownyEco.getDataSource().saveNPCList();

			} catch (AlreadyExistingException ex) {
				// Should never happen
			}

		}
	}
}
