package com.writedive.townyeco.tasks;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.exceptions.AlreadyExistingException;
import com.writedive.townyeco.objects.TEPlayer;
import org.bukkit.entity.Player;

public class PlayerLoginTask implements Runnable {
	private Player player;


	public PlayerLoginTask(Player player) {
		this.player = player;
	}

	@Override
	public void run() {

		TEPlayer tePlayer;

		if (!TownyEco.getDataSource().hasPlayer(player.getUniqueId())) {
			try {
				TownyEco.getDataSource().newPlayer(player.getUniqueId());
				tePlayer = TownyEco.getDataSource().getPlayer(player.getUniqueId());
				tePlayer.setRegistered(System.currentTimeMillis());

				TownyEco.getDataSource().savePlayer(tePlayer);
				TownyEco.getDataSource().savePlayerList();

			} catch (AlreadyExistingException ex) {
				// Should never happen
			}

		} else {
			/*
			 * This player is known so fetch the data and update it.
			 */
			tePlayer = TownyEco.getDataSource().getPlayer(player.getUniqueId());
			tePlayer.setLastOnline(System.currentTimeMillis());

			TownyEco.getDataSource().savePlayer(tePlayer);

		}
	}
}
