package com.writedive.townyeco.economy;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.messaging.TownyEcoLogger;
import com.writedive.townyeco.objects.TENpc;
import com.writedive.townyeco.objects.objectinterfaces.AccountHolder;
import com.writedive.townyeco.tasks.NPCRegisterTask;
import com.writedive.townyeco.utilities.BukkitTools;
import org.bukkit.Bukkit;

import java.util.UUID;

public class EconomyManager {
	private TownyEco plugin;
	private boolean enabled;

	public EconomyManager(TownyEco plugin) {
		this.plugin = plugin;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return is the EconomyManager still enabled.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param playername - Playername
	 * @return UUID of player.
	 */
	public UUID getUUIDFromPlayerName(String playername) {
		if (playername != null) {
			if (Bukkit.getOfflinePlayer(playername) != null) {
				return Bukkit.getOfflinePlayer(playername).getUniqueId();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * @param npcname - NPC name
	 * @return UUID of npc
	 */
	public UUID getUUIDFromNPCName(String npcname) {
		if (npcname != null) {
			for (TENpc npc : plugin.getNPCMap().values()) {
				if (npc.getName().equalsIgnoreCase(npcname)) {
					return npc.getUuid();
				}
			}
			return null;
		} else {
			return null;
		}
	}

	public AccountHolder getAccountHolder(String s) {
		if (getUUIDFromPlayerName(s) != null) {
			// Player exists.
			return getAccountHolder(getUUIDFromPlayerName(s));
		} else { // Player doesn't exist
			if (getUUIDFromNPCName(s) != null) {
				return getAccountHolder(getUUIDFromNPCName(s));
			} else {
				return null;
			}
		}
	}

	public AccountHolder getAccountHolder(UUID uuid) {
		if (uuid != null) {
			AccountHolder holder;
			try {
				holder = TownyEco.getDataSource().getPlayer(uuid);
			} catch (NullPointerException e) {
				try {
					holder = TownyEco.getDataSource().getNPC(uuid);
				} catch (NullPointerException f) {
					return null;
				}
			}
			return holder;
		} else {
			return null;
		}
	}

	public boolean createNPCAccount(String s) {
		if (s != null) {
			if (getAccountHolder(s) != null) {
				return true; // Already has an npc.
			}
			// Nothing with name (s) was in the for loop (map) so:
			return createNPCAccount(UUID.randomUUID());
		} else {
			return false;
		}
	}

	public boolean createNPCAccount(UUID uuid) {
		if (uuid != null) {
			if (BukkitTools.scheduleSyncDelayedTask(new NPCRegisterTask(uuid), 0L) == -1) {
				TownyEcoLogger.log("Couldn't create npc with UUID" + uuid, TownyEcoLogger.LogType.ECONOMY);
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}
