package com.writedive.townyeco.economy;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.configuration.ConfigurationHandler;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;

import java.util.List;

public class VaultEconomy implements Economy {
	private TownyEco plugin;
	private EconomyManager eco;

	public VaultEconomy(TownyEco plugin) {
		this.plugin = plugin;
		this.eco = plugin.getEconomy();
	}

	@Override
	public boolean isEnabled() {
		return eco.isEnabled();
	}

	@Override
	public String getName() {
		return plugin.getName();
	}

	@Override
	public boolean hasBankSupport() {
		return false;
	}

	@Override
	public int fractionalDigits() {
		return 2;
	}

	@Override
	public String format(double v) {
		return null;
	}

	@Override
	public String currencyNamePlural() {
		return ConfigurationHandler.getCurrencyNamePlural();
	}

	@Override
	public String currencyNameSingular() {
		return ConfigurationHandler.getCurrencyNameSingular();
	}

	@Override
	public boolean hasAccount(String s) {
		return eco.getAccountHolder(s) != null;
	}

	@Override
	public boolean hasAccount(OfflinePlayer offlinePlayer) {
		return eco.getAccountHolder(offlinePlayer.getUniqueId()) != null;
	}

	@Override
	public boolean hasAccount(String s, String s1) {
		return eco.getAccountHolder(s) != null;
	}

	@Override
	public boolean hasAccount(OfflinePlayer offlinePlayer, String s) {
		return eco.getAccountHolder(offlinePlayer.getUniqueId()) != null;
	}

	@Override
	public double getBalance(String s) {
		try {
			return eco.getAccountHolder(s).getBalance();
		} catch (NullPointerException e){
			return 0;
		}
	}

	@Override
	public double getBalance(OfflinePlayer offlinePlayer) {
		try {
			return eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance();
		} catch (NullPointerException e){
			return 0;
		}
	}

	@Override
	public double getBalance(String s, String s1) {
		try {
			return eco.getAccountHolder(s).getBalance();
		} catch (NullPointerException e){
			return 0;
		}
	}

	@Override
	public double getBalance(OfflinePlayer offlinePlayer, String s) {
		try {
			return eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance();
		} catch (NullPointerException e){
			return 0;
		}
	}

	@Override
	public boolean has(String s, double v) {
		try {
			return eco.getAccountHolder(s).getBalance() > v;
		} catch (NullPointerException e){
			return false;
		}
	}

	@Override
	public boolean has(OfflinePlayer offlinePlayer, double v) {
		try {
			return eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance() > v;
		} catch (NullPointerException e){
			return false;
		}
	}

	@Override
	public boolean has(String s, String s1, double v) {
		try {
			return eco.getAccountHolder(s).getBalance() > v;
		} catch (NullPointerException e){
			return false;
		}
	}

	@Override
	public boolean has(OfflinePlayer offlinePlayer, String s, double v) {
		try {
			return eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance() > v;
		} catch (NullPointerException e){
			return false;
		}
	}

	@Override
	public EconomyResponse withdrawPlayer(String s, double v) {
		try {
			eco.getAccountHolder(s).removeFromBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(s).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, double v) {
		try {
			eco.getAccountHolder(offlinePlayer.getUniqueId()).removeFromBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse withdrawPlayer(String s, String s1, double v) {
		try {
			eco.getAccountHolder(s).removeFromBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(s).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, String s, double v) {
		try {
			eco.getAccountHolder(offlinePlayer.getUniqueId()).removeFromBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse depositPlayer(String s, double v) {
		try {
			eco.getAccountHolder(s).addToBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(s).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, double v) {
		try {
			eco.getAccountHolder(offlinePlayer.getUniqueId()).addToBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse depositPlayer(String s, String s1, double v) {
		try {
			eco.getAccountHolder(s).addToBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(s).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, String s, double v) {
		try {
			eco.getAccountHolder(offlinePlayer.getUniqueId()).addToBalance(v);
			return new EconomyResponse(v, eco.getAccountHolder(offlinePlayer.getUniqueId()).getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
		} catch (NullPointerException e){
			return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, null);
		}
	}

	@Override
	public EconomyResponse createBank(String s, String s1) {
		return null;
	}

	@Override
	public EconomyResponse createBank(String s, OfflinePlayer offlinePlayer) {
		return null;
	}

	@Override
	public EconomyResponse deleteBank(String s) {
		return null;
	}

	@Override
	public EconomyResponse bankBalance(String s) {
		return null;
	}

	@Override
	public EconomyResponse bankHas(String s, double v) {
		return null;
	}

	@Override
	public EconomyResponse bankWithdraw(String s, double v) {
		return null;
	}

	@Override
	public EconomyResponse bankDeposit(String s, double v) {
		return null;
	}

	@Override
	public EconomyResponse isBankOwner(String s, String s1) {
		return null;
	}

	@Override
	public EconomyResponse isBankOwner(String s, OfflinePlayer offlinePlayer) {
		return null;
	}

	@Override
	public EconomyResponse isBankMember(String s, String s1) {
		return null;
	}

	@Override
	public EconomyResponse isBankMember(String s, OfflinePlayer offlinePlayer) {
		return null;
	}

	@Override
	public List<String> getBanks() {
		return null;
	}

	@Override
	public boolean createPlayerAccount(String s) {
		return eco.createNPCAccount(s);
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer offlinePlayer) {
		return true;
	}

	@Override
	public boolean createPlayerAccount(String s, String s1) {
		return eco.createNPCAccount(s);
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer offlinePlayer, String s) {
		return true;
	}
}
