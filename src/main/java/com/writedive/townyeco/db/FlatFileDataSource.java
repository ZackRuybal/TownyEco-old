package com.writedive.townyeco.db;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.exceptions.AlreadyExistingException;
import com.writedive.townyeco.messaging.TownyEcoLogger;
import com.writedive.townyeco.objects.TENpc;
import com.writedive.townyeco.objects.TEPlayer;
import com.writedive.townyeco.utilities.BukkitTools;
import com.writedive.util.FileMgmt;
import com.writedive.util.KeyValueFile;
import org.bukkit.scheduler.BukkitTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.writedive.townyeco.messaging.TownyEcoLogger.LogType.DATABASE;

/**
 * @author - Articdive
 */
public class FlatFileDataSource extends DataHandler {

	protected String dataFolder = "";
	private BukkitTask task = null;
	private Queue<FlatFile_Task> queryQueue = new ConcurrentLinkedQueue<>();

	@Override
	public void initialize(TownyEco plugin) {
		this.plugin = plugin;
		rootFolder = plugin.getRootFolder();
		settingsFolder = rootFolder + FileMgmt.fileSeparator() + "settings";
		logFolder = rootFolder + FileMgmt.fileSeparator() + "logs";
		dataFolder = rootFolder + FileMgmt.fileSeparator() + "data";
		try {
			FileMgmt.checkFolders(new String[]{
					rootFolder,
					dataFolder,
					dataFolder + FileMgmt.fileSeparator() + "players",
					dataFolder + FileMgmt.fileSeparator() + "npcs"});
			FileMgmt.checkFiles(new String[]{
					dataFolder + FileMgmt.fileSeparator() + "players.txt",
					dataFolder + FileMgmt.fileSeparator() + "npcs.txt",});
		} catch (IOException e) {
			TownyEcoLogger.log("Could not create flatfile default files and folders", TownyEcoLogger.LogType.DATABASE);
		}
		/*
		 * Start our Async queue for pushing data to the database.
		 */
		task = BukkitTools.getScheduler().runTaskTimerAsynchronously(plugin, () -> {

			while (!FlatFileDataSource.this.queryQueue.isEmpty()) {

				FlatFile_Task query = FlatFileDataSource.this.queryQueue.poll();

				try {

					FileMgmt.listToFile(query.list, query.path);

				} catch (IOException e) {

					TownyEcoLogger.log("Error saving file - " + query.path, DATABASE);

				} catch (NullPointerException ex) {

					TownyEcoLogger.log("Null Error saving to file - " + query.path, DATABASE);

				}

			}

		}, 5L, 5L);
	}

	@Override
	public void cancelTask() {
		task.cancel();
	}

	@Override
	public boolean loadPlayerList() {
		TownyEcoLogger.log("Loading Player List", DATABASE);
		String line = null;
		BufferedReader fin = null;

		try {
			fin = new BufferedReader(new FileReader(dataFolder + FileMgmt.fileSeparator() + "players.txt"));

			while ((line = fin.readLine()) != null)
				if (!line.equals(""))
					newPlayer(UUID.fromString(line));

			return true;

		} catch (AlreadyExistingException e) {
			TownyEcoLogger.log("Error Loading Player List at " + line + ", player possibly listed twice.", DATABASE);
			e.printStackTrace();
			return false;

		} catch (Exception e) {
			TownyEcoLogger.log("Error Loading Player List at " + line + ", in players.txt", DATABASE);
			e.printStackTrace();
			return false;

		} finally {
			if (fin != null) {
				try {
					fin.close();
				} catch (IOException ignore) {
				}
			}
		}
	}

	@Override
	public boolean loadNPCList() {
		TownyEcoLogger.log("Loading NPC List", DATABASE);
		String line = null;
		BufferedReader fin = null;

		try {
			fin = new BufferedReader(new FileReader(dataFolder + FileMgmt.fileSeparator() + "npcs.txt"));

			while ((line = fin.readLine()) != null)
				if (!line.equals(""))
					newNPC(UUID.fromString(line));

			return true;

		} catch (AlreadyExistingException e) {
			TownyEcoLogger.log("Error Loading NPC List at " + line + ", npc is possibly listed twice.", DATABASE);
			e.printStackTrace();
			return false;

		} catch (Exception e) {
			TownyEcoLogger.log("Error Loading NPC List at " + line + ", in npcs.txt", DATABASE);
			e.printStackTrace();
			return false;

		} finally {
			if (fin != null) {
				try {
					fin.close();
				} catch (IOException ignore) {
				}
			}
		}
	}

	@Override
	public boolean savePlayerList() {
		List<String> list = new ArrayList<>();

		for (TEPlayer player : getPlayers()) {

			list.add(player.getUuid().toString());
		}

		/*
		 *  Make sure we only save in async
		 */
		this.queryQueue.add(new FlatFile_Task(list, dataFolder + FileMgmt.fileSeparator() + "players.txt"));

		return true;
	}

	@Override
	public boolean saveNPCList() {
		List<String> list = new ArrayList<>();

		for (TENpc npc : getNPCs()) {

			list.add(npc.getUuid().toString());
		}

		/*
		 *  Make sure we only save in async
		 */
		this.queryQueue.add(new FlatFile_Task(list, dataFolder + FileMgmt.fileSeparator() + "npcs.txt"));

		return true;
	}

	@Override
	public boolean loadPlayer(TEPlayer player) {
		String line = null;
		String path = getPlayerFilePath(player);
		File fileResident = new File(path);
		if (fileResident.exists() && fileResident.isFile()) {
			try {
				KeyValueFile kvFile = new KeyValueFile(path);
				line = kvFile.get("lastonline");
				player.setLastOnline(Long.parseLong(line));
				line = kvFile.get("registered");
				player.setRegistered(Long.parseLong(line));
				line = kvFile.get("balance");
				player.setBalance(Double.parseDouble(line));

			} catch (Exception e) {
				TownyEcoLogger.log("Loading Error: Exception while reading player file " + player.getUuid() + " at line: " + line + ", in townyeco\\data\\players\\" + player.getUuid() + ".txt", DATABASE);
				return false;
			}

			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean loadNPC(TENpc npc) {
		String line = null;
		String path = getNPCFilePath(npc);
		File fileResident = new File(path);
		if (fileResident.exists() && fileResident.isFile()) {
			try {
				KeyValueFile kvFile = new KeyValueFile(path);
				line = kvFile.get("name");
				npc.setName(line);
				line = kvFile.get("registered");
				npc.setRegistered(Long.parseLong(line));
				line = kvFile.get("balance");
				npc.setBalance(Double.parseDouble(line));
			} catch (Exception e) {
				TownyEcoLogger.log("Loading Error: Exception while reading npc file " + npc.getUuid() + " at line: " + line + ", in townyeco\\data\\npcs\\" + npc.getUuid() + ".txt", DATABASE);
				return false;
			}

			return true;
		} else
			return false;
	}

	@Override
	public boolean savePlayer(TEPlayer player) {
		List<String> list = new ArrayList<>();

		// UUID
		list.add("uuid=" + player.getUuid().toString());
		// LastOnline
		list.add("lastonline=" + Long.toString(player.getLastOnline()));
		// Registration Date
		list.add("registered=" + Long.toString(player.getRegistered()));

		list.add("balance=" + Double.toString(player.getBalance()));

		/*
		 *  Make sure we only save in async
		 */
		this.queryQueue.add(new FlatFile_Task(list, getPlayerFilePath(player)));

		return true;
	}

	@Override
	public boolean saveNPC(TENpc npc) {
		List<String> list = new ArrayList<>();

		// UUID
		list.add("uuid=" + npc.getUuid().toString());
		// Name
		list.add("name=" + npc.getName());
		// Registration Date
		list.add("registered=" + Long.toString(npc.getRegistered()));
		// Balance
		list.add("balance=" + Double.toString(npc.getBalance()));
		/*
		 *  Make sure we only save in async
		 */
		this.queryQueue.add(new FlatFile_Task(list, getNPCFilePath(npc)));

		return true;
	}

	@Override
	public boolean backup() {
		return false;
	}

	@Override
	public boolean clearBackups() {
		return false;
	}

	@Override
	public void deletePlayer(TEPlayer player) {
		File file = new File(getPlayerFilePath(player));
		if (file.exists()) {
			file.delete();
		}
	}

	@Override
	public void deleteNPC(TENpc npc) {
		File file = new File(getNPCFilePath(npc));
		if (file.exists()) {
			file.delete();
		}
	}

	/**
	 * Gets the filepath to the player's file.
	 *
	 * @param player - TEPlayer Object
	 * @return - Path of the TEPlayer's storage file.
	 */
	public String getPlayerFilePath(TEPlayer player) {

		return dataFolder + FileMgmt.fileSeparator() + "players" + FileMgmt.fileSeparator() + player.getUuid() + ".txt";
	}

	/**
	 * Gets the filepath to the NPC's file
	 *
	 * @param npc - TENpc Object
	 * @return - Path of NPC's storage file
	 */
	public String getNPCFilePath(TENpc npc) {

		return dataFolder + FileMgmt.fileSeparator() + "npcs" + FileMgmt.fileSeparator() + npc.getUuid() + ".txt";
	}
}
