package com.writedive.townyeco.db;

import com.writedive.townyeco.configuration.ConfigurationHandler;
import com.writedive.townyeco.messaging.TownyEcoLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ElgarL
 */
public class SQL_Schema {

	private static String tb_prefix = ConfigurationHandler.getSQLTablePrefix().toUpperCase();

	private static String getPlayers() {

		return "CREATE TABLE IF NOT EXISTS " + tb_prefix + "PLAYERS ("
				+ "`uuid` VARCHAR(36) NOT NULL,"
				+ "PRIMARY KEY (`uuid`)"
				+ ")";
	}

	private static String getNPCs() {

		return "CREATE TABLE IF NOT EXISTS " + tb_prefix + "NPCS ("
				+ "`uuid` VARCHAR(36) NOT NULL,"
				+ "PRIMARY KEY (`uuid`)"
				+ ")";
	}

	public static List<String> getPlayerValues() {
		List<String> values = new ArrayList<>();
		values.add("`registered` BIGINT NOT NULL");
		values.add("`lastonline` BIGINT DEFAULT NULL");
		values.add("`balance` FLOAT(53) NOT NULL");
		return values;
	}


	public static List<String> getNPCValues() {
		List<String> values = new ArrayList<>();
		values.add("`name` VARCHAR(80) DEFAULT NULL");
		values.add("`registered` BIGINT NOT NULL");
		values.add("`balance` FLOAT(53) NOT NULL");
		return values;
	}

	/**
	 * Create and update database schema.
	 *
	 * @param cntx    a database connection
	 * @param db_name the name of a database
	 */
	public static void initTables(Connection cntx, String db_name) {

		/*
		 *  Fetch PLAYERS Table schema.
		 */
		String players_create = getPlayers();

		try {

			Statement s = cntx.createStatement();
			s.executeUpdate(players_create);


			TownyEcoLogger.log("Fetching Player Schema", TownyEcoLogger.LogType.DATABASE);

		} catch (SQLException ee) {

			TownyEcoLogger.log("SQLError:" + ee.getMessage(), TownyEcoLogger.LogType.DATABASE);

		}

		/*
		 *  Fetch NPCS Table schema.
		 */
		String npcs_create = getNPCs();

		try {

			Statement s = cntx.createStatement();
			s.executeUpdate(npcs_create);

			TownyEcoLogger.log("Fetching NPC Schema", TownyEcoLogger.LogType.DATABASE);

		} catch (SQLException ee) {

			TownyEcoLogger.log("SQLError:" + ee.getMessage(), TownyEcoLogger.LogType.DATABASE);

		}



		/*
		 * Update the table structures add all columns.
		 *
		 * Update PLAYERS.
		 */
		String player_update;
		List<String> townsSQLSchema_players = getPlayerValues();
		for (String mysqlvalue : townsSQLSchema_players) {
			try {
				player_update = "ALTER TABLE `" + db_name + "`.`" + tb_prefix + "PLAYERS` "
						+ "ADD COLUMN " + mysqlvalue;

				PreparedStatement ps = cntx.prepareStatement(player_update);
				ps.executeUpdate();

			} catch (SQLException ee) {
				if (ee.getErrorCode() != 1060) ;
				TownyEcoLogger.log("SQLError:" + ee.getMessage(), TownyEcoLogger.LogType.DATABASE);
			}
		}
		TownyEcoLogger.log("Updating Player Table", TownyEcoLogger.LogType.DATABASE);

		/*
		 * Update the table structures add all columns.
		 *
		 * Update NPCS.
		 */
		String npc_update;
		List<String> townsSQLSchema_npcs = getNPCValues();
		for (String mysqlvalue : townsSQLSchema_npcs) {
			try {
				npc_update = "ALTER TABLE `" + db_name + "`.`" + tb_prefix + "NPCS` "
						+ "ADD COLUMN " + mysqlvalue;

				PreparedStatement ps = cntx.prepareStatement(npc_update);
				ps.executeUpdate();

			} catch (SQLException ee) {
				if (ee.getErrorCode() != 1060) ;
				TownyEcoLogger.log("SQLError:" + ee.getMessage(), TownyEcoLogger.LogType.DATABASE);
			}
		}
		TownyEcoLogger.log("Updating NPC Table", TownyEcoLogger.LogType.DATABASE);


	}
}
