package com.writedive.townyeco.db;

import com.writedive.townyeco.exceptions.AlreadyExistingException;
import com.writedive.townyeco.objects.TENpc;
import com.writedive.townyeco.objects.TEPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author - Articdive
 */
public abstract class DataHandler extends DataSource {
	@Override
	public List<TEPlayer> getPlayers() {
		return new ArrayList<>(plugin.getPlayerMap().values());
	}

	@Override
	public List<TENpc> getNPCs() {
		return new ArrayList<>(plugin.getNPCMap().values());
	}

	@Override
	public TEPlayer getPlayer(UUID uuid) {
		return plugin.getPlayerMap().get(uuid);
	}

	@Override
	public TENpc getNPC(UUID uuid) {
		return plugin.getNPCMap().get(uuid);
	}

	@Override
	public boolean hasPlayer(UUID uuid) {
		if (plugin.getPlayerMap().containsKey(uuid)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean hasNPC(UUID uuid) {
		if (plugin.getNPCMap().containsKey(uuid)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void removePlayerfromList(TEPlayer player) {

		// Save the player one last time.
		savePlayer(player);

		// Delete the player.
		deletePlayer(player);
		// Remove the player from the playermap.
		plugin.getPlayerMap().remove(player.getUuid());

		// Save the new List, (without the player now)
		savePlayerList();
	}

	@Override
	public void removeNPCfromList(TENpc npc) {

		// Save the npc one last time.
		saveNPC(npc);

		// Delete the player.
		deleteNPC(npc);
		// Remove the player from the playermap.
		plugin.getPlayerMap().remove(npc.getUuid());

		// Save the new List, (without the npc now)
		saveNPCList();

	}

	@Override
	public void newPlayer(UUID uuid) throws AlreadyExistingException {
		if (plugin.getPlayerMap().containsKey(uuid)) {
			throw new AlreadyExistingException("A player with the uuid " + uuid + " is already in use.");
		}

		plugin.getPlayerMap().put(uuid, new TEPlayer(uuid));
	}

	@Override
	public void newNPC(UUID uuid) throws AlreadyExistingException {
		if (plugin.getNPCMap().containsKey(uuid)) {
			throw new AlreadyExistingException("A npc with the uuid " + uuid + " is already in use.");
		}

		plugin.getNPCMap().put(uuid, new TENpc(uuid));

	}
}
