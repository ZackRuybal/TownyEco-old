package com.writedive.townyeco.db;

import java.util.List;

/**
 * @author - Articdive
 */
public class FlatFile_Task {

	public final String path;
	public List<String> list;

	/**
	 * Constructor to save a list
	 *
	 * @param list - List of variables
	 * @param path - Path of where to put those variables
	 */
	public FlatFile_Task(List<String> list, String path) {
		this.list = list;
		this.path = path;
	}
}