package com.writedive.townyeco.db;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.messaging.TownyEcoLogger;
import com.writedive.townyeco.exceptions.AlreadyExistingException;
import com.writedive.townyeco.objects.TENpc;
import com.writedive.townyeco.objects.TEPlayer;
import com.writedive.util.FileMgmt;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.writedive.townyeco.messaging.TownyEcoLogger.LogType.DATABASE;

/**
 * @author - Articdive
 */
public abstract class DataSource {
	// This is where all methods are handled in databases, all databases must extend this indirectly
	// Always do LOAD > SAVE, that means in order LOAD will always come first.
	protected TownyEco plugin;
	protected String rootFolder;
	protected String settingsFolder;
	protected String logFolder;

	// We need an initializer;
	public void initialize(TownyEco plugin) {
		this.plugin = plugin;
		rootFolder = plugin.getRootFolder();
		settingsFolder = rootFolder + FileMgmt.fileSeparator() + "settings";
		logFolder = rootFolder + FileMgmt.fileSeparator() + "logs";

	}

	// We will be using asynced tasks to load/save stuff, we need those to be cancellable

	/**
	 * Allows you to cancel the task, stop writing to DB
	 */
	abstract public void cancelTask();

	// Backup Functions

	/**
	 * Backs your DB up.
	 *
	 * @return false - if something went wrong while backing up.
	 */
	abstract public boolean backup();

	/**
	 * Deletes all your backups
	 *
	 * @return false - if something went wrong while deleting your backups (unlikely)
	 */
	abstract public boolean clearBackups();

	// We also need a loadall and saveall:


	/**
	 * Loads all objects,
	 *
	 * @return false - if anything went wrong while loading
	 */
	public boolean loadAll() {

		return loadPlayerList() && loadNPCList() && loadPlayers() && loadNPCs();
	}

	/**
	 * Saves all objects,
	 *
	 * @return false - if anything went wrong while saving
	 */
	public boolean saveAll() {
		return savePlayerList() && saveNPCList() && savePlayers() && saveNPCs();
	}

	// List of list-items:

	/**
	 * Loads a list of players
	 *
	 * @return false - if anything went wrong while loading
	 */
	abstract public boolean loadPlayerList();

	/**
	 * Loads a list of npcs
	 *
	 * @return false - if anything went wrong while loading
	 */
	abstract public boolean loadNPCList();

	/**
	 * Saves a list of players
	 *
	 * @return false - if anything went wrong while saving
	 */
	abstract public boolean savePlayerList();

	/**
	 * Saves a list of npcs
	 *
	 * @return false - if anything went wrong while saving
	 */
	abstract public boolean saveNPCList();

	// Once we have the list of players, we can load the players, same with npcs
	// List of objects-items:

	/**
	 * Loads a player from the DB
	 *
	 * @param player - Must be a TEPLayer objects
	 * @return false - if something went wrong while loading.
	 */
	abstract public boolean loadPlayer(TEPlayer player);

	/**
	 * Loads a npc from the DB
	 *
	 * @param npc - Must be a TENpc objects
	 * @return false - if something went wrong while loading.
	 */
	abstract public boolean loadNPC(TENpc npc);

	/**
	 * Saves player into the DB
	 *
	 * @param player - Must be a TEPLayer objects
	 * @return false - if something went wrong while saving.
	 */
	abstract public boolean savePlayer(TEPlayer player);

	/**
	 * Saves a npc into the DB
	 *
	 * @param npc - Must be a TENpc objects
	 * @return false - if something went wrong while saving.
	 */
	abstract public boolean saveNPC(TENpc npc);

	// Database Handling methods (in DataHandler),
	// getters:

	/**
	 * Get a list of registered, Players (may be invalid)
	 *
	 * @return List of TEPlayer, which TownyEco has registered in the DB.
	 */
	abstract public List<TEPlayer> getPlayers();

	/**
	 * Get a list of registered, NPCs (may be invalid)
	 *
	 * @return List of TENpc, which TownyEco has registered in the DB.
	 */
	abstract public List<TENpc> getNPCs();

	/**
	 * Get a TEPlayer object via it's UUID
	 *
	 * @param uuid - UUID of the object.
	 * @return - TEPlayer with said UUID
	 */
	abstract public TEPlayer getPlayer(UUID uuid);

	/**
	 * Get a TENpc object via it's UUID
	 *
	 * @param uuid - UUID of the object.
	 * @return - TENpcwith said UUID
	 */
	abstract public TENpc getNPC(UUID uuid);

	// List removers:

	/**
	 * Removes a player from the player list.
	 *
	 * @param player - TEPlayer to be removed from the TEPlayerList
	 */
	abstract public void removePlayerfromList(TEPlayer player);

	/**
	 * Removes a npc from the player list.
	 *
	 * @param npc - TENpc to be removed from the TENpcList
	 */
	abstract public void removeNPCfromList(TENpc npc);

	/**
	 * Checks if the DB contains a player with that uuid
	 *
	 * @return - true if the DB contains a player with that uuid.
	 */
	abstract public boolean hasPlayer(UUID uuid);

	/**
	 * Checks if the DB contains a npc with that uuid
	 *
	 * @return - true if the DB contains a npc with that uuid.
	 */
	abstract public boolean hasNPC(UUID uuid);

	// Object-item creators:

	/**
	 * Creates a new Player Object in the DB.
	 *
	 * @param uuid - UUID of the TEPlayer
	 * @throws AlreadyExistingException - If UUID already registered
	 */
	abstract public void newPlayer(UUID uuid) throws AlreadyExistingException;

	/**
	 * Creates a new Player Object in the DB.
	 *
	 * @param uuid - UUID of the TENpc (Should be created if new)
	 * @throws AlreadyExistingException - If UUID already registered & exists. (unlikely)
	 */
	abstract public void newNPC(UUID uuid) throws AlreadyExistingException;

	// Object-item deleters:

	/**
	 * Delete a Player from the DB.
	 *
	 * @param player - TEPlayer objects to be deleted.
	 */
	abstract public void deletePlayer(TEPlayer player);

	/**
	 * Delete a Npc from the DB.
	 *
	 * @param npc - TENpc objects to be deleted.
	 */
	abstract public void deleteNPC(TENpc npc);

	// We also have some methods which always do the same no matter what:
	// These methods are to load all objects, they get each objects individually and load them.

	/**
	 * Loads all players from the playerlist
	 *
	 * @return - Always true
	 */
	public boolean loadPlayers() {
		TownyEcoLogger.log("Loading Players", DATABASE);

		List<TEPlayer> toRemove = new ArrayList<>();

		for (TEPlayer player : new ArrayList<>(getPlayers()))
			if (!loadPlayer(player)) {
				TownyEcoLogger.log("Loading Error: Could not read player data '" + player.getUuid() + "'.", DATABASE);
				toRemove.add(player);
				//return false;
			}

		// Remove any resident which failed to load.
		for (TEPlayer player : toRemove) {
			TownyEcoLogger.log("Loading Error: Removing player data for '" + player.getUuid() + "'.", DATABASE);
			removePlayerfromList(player);
		}
		return true;
	}

	/**
	 * Loads all npcs from the npclist
	 *
	 * @return - Always true
	 */
	public boolean loadNPCs() {

		TownyEcoLogger.log("Starting load of NPCs", DATABASE);

		List<TENpc> toRemove = new ArrayList<>();

		for (TENpc npc : new ArrayList<>(getNPCs()))
			if (!loadNPC(npc)) {
				TownyEcoLogger.log("Loading Error: Could not read NPC data '" + npc.getUuid() + "'.", DATABASE);
				toRemove.add(npc);
			}

		// Remove any resident which failed to load.
		for (TENpc npc : toRemove) {
			TownyEcoLogger.log("Loading Error: Removing NPC Data for '" + npc.getUuid() + "'.", DATABASE);
			removeNPCfromList(npc);
		}
		return true;
	}

	/**
	 * Saves all Players from the playerlist
	 *
	 * @return - Always true
	 */
	public boolean savePlayers() {

		TownyEcoLogger.log("Saving Players", DATABASE);
		for (TEPlayer player : getPlayers()) {
			savePlayer(player);
		}
		return true;
	}

	/**
	 * Saves all npcs from the npclist
	 *
	 * @return - Always true
	 */
	public boolean saveNPCs() {

		TownyEcoLogger.log("Saving NPCs", DATABASE);
		for (TENpc npc : getNPCs()) {
			saveNPC(npc);
		}
		return true;
	}
}
