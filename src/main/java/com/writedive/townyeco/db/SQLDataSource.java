package com.writedive.townyeco.db;


import com.palmergames.bukkit.towny.TownyMessaging;
import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.configuration.ConfigurationHandler;
import com.writedive.townyeco.messaging.TownyEcoLogger;
import com.writedive.townyeco.objects.TENpc;
import com.writedive.townyeco.objects.TEPlayer;
import com.writedive.townyeco.utilities.BukkitTools;
import com.writedive.util.FileMgmt;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.writedive.townyeco.messaging.TownyEcoLogger.LogType.DATABASE;

public class SQLDataSource extends FlatFileDataSource {
	protected String driver = "";
	protected String dsn = "";
	protected String hostname = "";
	protected String port = "";
	protected String db_name = "";
	protected String username = "";
	protected String password = "";
	protected String tb_prefix = "";
	private Queue<SQL_Task> queryQueue = new ConcurrentLinkedQueue<>();
	private BukkitTask task = null;
	private Connection cntx = null;
	private String type;

	// private boolean ish2 = false;

	/**
	 * Flag if we are using h2 or standard SQL connectivity.
	 *
	 * @param type
	 */
	public SQLDataSource(String type) {

		this.type = type.toLowerCase();
	}

	@Override
	public void cancelTask() {
		task.cancel();
	}

	@Override
	public void initialize(TownyEco plugin) {

		this.plugin = plugin;
		this.rootFolder = plugin.getRootFolder();
		settingsFolder = rootFolder + FileMgmt.fileSeparator() + "settings";
		logFolder = rootFolder + FileMgmt.fileSeparator() + "logs";
		dataFolder = rootFolder + FileMgmt.fileSeparator() + "data";


		/*
		 *  Setup SQL connection
		 */
		hostname = ConfigurationHandler.getSQLHostName();
		port = ConfigurationHandler.getSQLPort();
		db_name = ConfigurationHandler.getSQLDBName();
		tb_prefix = ConfigurationHandler.getSQLTablePrefix().toUpperCase();

		if (this.type.equals("h2")) {

			this.driver = "org.h2.Driver";
			this.dsn = ("jdbc:h2:" + rootFolder + dataFolder + File.separator + db_name + ".h2db;AUTO_RECONNECT=TRUE");
			username = "sa";
			password = "sa";

		} else if (this.type.equals("mysql")) {

			this.driver = "com.mysql.jdbc.Driver";
			if (ConfigurationHandler.getSQLUsingSSL())
				this.dsn = ("jdbc:mysql://" + hostname + ":" + port + "/" + db_name + "?useUnicode=true&characterEncoding=utf-8");
			else
				this.dsn = ("jdbc:mysql://" + hostname + ":" + port + "/" + db_name + "?verifyServerCertificate=false&useSSL=false&useUnicode=true&characterEncoding=utf-8");
			username = ConfigurationHandler.getSQLUsername();
			password = ConfigurationHandler.getSQLPassword();

		} else {

			this.driver = "org.sqlite.JDBC";
			this.dsn = ("jdbc:sqlite:" + rootFolder + dataFolder + File.separator + db_name + ".sqldb");
			username = "";
			password = "";

		}

		/*
		 * Register the driver (if possible)
		 */
		try {
			Driver driver = (Driver) Class.forName(this.driver).newInstance();
			DriverManager.registerDriver(driver);
		} catch (Exception e) {
			TownyEcoLogger.log("SQLError: " + e.getMessage() , TownyEcoLogger.LogType.DATABASE);
		}

		/*
		 * Attempt to get a connection to the database
		 */
		if (getContext()) {

			TownyEcoLogger.log("Establish connection to database" , TownyEcoLogger.LogType.DATABASE);

		} else {


			TownyEcoLogger.log("SQLError: Failed to establish connection" , TownyEcoLogger.LogType.DATABASE);
			return;

		}

		/*
		 *  Initialise database Schema.
		 */
		SQL_Schema.initTables(cntx, db_name);

		/*
		 * Start our Async queue for pushing data to the database.
		 */
		task = BukkitTools.getScheduler().runTaskTimerAsynchronously(plugin, () -> {

			while (!SQLDataSource.this.queryQueue.isEmpty()) {

				SQL_Task query = SQLDataSource.this.queryQueue.poll();

				if (query.update) {

					SQLDataSource.this.QueueUpdateDB(query.tb_name, query.args, query.keys);

				} else {

					SQLDataSource.this.QueueDeleteDB(query.tb_name, query.args);

				}

			}

		}, 5L, 5L);
	}

	/**
	 * open a connection to the SQL server.
	 *
	 * @return true if we successfully connected to the db.
	 */
	public boolean getContext() {

		try {
			if (cntx == null || cntx.isClosed() || (!this.type.equals("sqlite") && !cntx.isValid(1))) {

				if (cntx != null && !cntx.isClosed()) {

					try {

						cntx.close();

					} catch (SQLException e) {
						/*
						 *  We're disposing of an old stale connection just be nice to the GC
						 *  as well as mysql, so ignore the error as there's nothing we can do
						 *  if it fails
						 */
					}
					cntx = null;
				}

				if ((this.username.equalsIgnoreCase("")) && (this.password.equalsIgnoreCase(""))) {

					cntx = DriverManager.getConnection(this.dsn);

				} else {

					cntx = DriverManager.getConnection(this.dsn, this.username, this.password);
				}

				if (cntx == null || cntx.isClosed())
					return false;
			}

			return true;

		} catch (SQLException e) {
			TownyEcoLogger.log("SQLError: Failed to establish connection" , TownyEcoLogger.LogType.DATABASE);
		}

		return false;
	}

	/**
	 * Build the SQL string and execute to INSERT/UPDATE
	 *
	 * @param tb_name
	 * @param args
	 * @param keys
	 * @return true if the update was successful.
	 */
	public boolean UpdateDB(String tb_name, HashMap<String, Object> args, List<String> keys) {

		/*
		 *  Make sure we only execute queries in async
		 */

		this.queryQueue.add(new SQL_Task(tb_name, args, keys));

		return true;


	}

	public boolean QueueUpdateDB(String tb_name, HashMap<String, Object> args, List<String> keys) {

		/*
		 *  Attempt to get a database connection.
		 */
		if (!getContext())
			return false;

		StringBuilder code;
		PreparedStatement stmt = null;
		List<Object> parameters = new ArrayList<>();
		int rs = 0;

		try {

			if (keys == null) {

				/*
				 * No keys so this is an INSERT not an UPDATE.
				 */

				// Push all values to a parameter list.

				parameters.addAll(args.values());

				String[] aKeys = args.keySet().toArray(new String[args.keySet().size()]);

				// Build the prepared statement string appropriate for
				// the number of keys/values we are inserting.

				code = new StringBuilder("REPLACE INTO " + tb_prefix + (tb_name.toUpperCase()) + " ");
				StringBuilder keycode = new StringBuilder("(");
				StringBuilder valuecode = new StringBuilder(" VALUES (");

				for (int count = 0; count < args.size(); count++) {

					keycode.append("`").append(aKeys[count]).append("`");
					valuecode.append("?");

					if ((count < (args.size() - 1))) {
						keycode.append(", ");
						valuecode.append(",");
					} else {
						keycode.append(")");
						valuecode.append(")");
					}
				}

				code.append(keycode);
				code.append(valuecode);

			} else {

				/*
				 * We have keys so this is a conditional UPDATE.
				 */

				String[] aKeys = args.keySet().toArray(new String[args.keySet().size()]);

				// Build the prepared statement string appropriate for
				// the number of keys/values we are inserting.

				code = new StringBuilder("UPDATE " + tb_prefix + (tb_name.toUpperCase()) + " SET ");

				for (int count = 0; count < args.size(); count++) {

					code.append("`").append(aKeys[count]).append("` = ?");

					// Push value for each entry.

					parameters.add(args.get(aKeys[count]));

					if ((count < (args.size() - 1))) {
						code.append(",");
					}
				}

				code.append(" WHERE ");

				for (int count = 0; count < keys.size(); count++) {

					code.append("`").append(keys.get(count)).append("` = ?");

					// Add extra values for the WHERE conditionals.

					parameters.add(args.get(keys.get(count)));

					if ((count < (keys.size() - 1))) {
						code.append(" AND ");
					}
				}

			}

			// Populate the prepared statement parameters.

			stmt = cntx.prepareStatement(code.toString());

			for (int count = 0; count < parameters.size(); count++) {

				Object element = parameters.get(count);

				if (element instanceof String) {

					stmt.setString(count + 1, (String) element);

				} else if (element instanceof Boolean) {

					stmt.setString(count + 1, ((Boolean) element) ? "1" : "0");

				} else {

					stmt.setObject(count + 1, element.toString());

				}

			}

			rs = stmt.executeUpdate();

		} catch (SQLException e) {

			TownyEcoLogger.log("SQLError:" + e.getMessage(), TownyEcoLogger.LogType.DATABASE);

		} finally {

			try {

				if (stmt != null) {
					stmt.close();
				}

				if (rs == 0) // if entry doesn't exist then try to insert
					return UpdateDB(tb_name, args, null);

			} catch (SQLException e) {
				TownyEcoLogger.log("SQLError:" + e.getMessage(), TownyEcoLogger.LogType.DATABASE);
			}

		}

		// Failed?
		if (rs == 0)
			return false;

		// Success!
		return true;

	}

	/**
	 * Build the SQL string and execute to DELETE
	 *
	 * @param tb_name
	 * @param args
	 * @return true if the delete was a success.
	 */
	public boolean DeleteDB(String tb_name, HashMap<String, Object> args) {

		// Make sure we only execute queries in async

		this.queryQueue.add(new SQL_Task(tb_name, args));

		return true;


	}

	public boolean QueueDeleteDB(String tb_name, HashMap<String, Object> args) {

		if (!getContext())
			return false;
		try {
			StringBuilder wherecode = new StringBuilder("DELETE FROM " + tb_prefix + (tb_name.toUpperCase()) + " WHERE ");
			Set<Map.Entry<String, Object>> set = args.entrySet();
			Iterator<Map.Entry<String, Object>> i = set.iterator();
			while (i.hasNext()) {
				Map.Entry<String, Object> me = i.next();
				wherecode.append("`").append(me.getKey()).append("` = ");
				if (me.getValue() instanceof String)
					wherecode.append("'").append(((String) me.getValue()).replace("'", "\''")).append("'");
				else if (me.getValue() instanceof Boolean)
					wherecode.append("'").append(((Boolean) me.getValue()) ? "1" : "0").append("'");
				else
					wherecode.append("'").append(me.getValue()).append("'");

				wherecode.append(i.hasNext() ? " AND " : "");
			}
			Statement s = cntx.createStatement();
			int rs = s.executeUpdate(wherecode.toString());
			s.close();
			if (rs == 0) {
				TownyEcoLogger.log("SQL: delete returned 0: " + wherecode, DATABASE);
			}
		} catch (SQLException e) {
			TownyEcoLogger.log("SQLError:" + e.getMessage(), TownyEcoLogger.LogType.DATABASE);
		}
		return false;
	}

	@Override
	public boolean loadPlayerList() {

		TownyEcoLogger.log("Loading Player List", DATABASE);
		if (!getContext())
			return false;
		try {
			Statement s = cntx.createStatement();
			ResultSet rs = s.executeQuery("SELECT uuid FROM " + tb_prefix + "PLAYERS");

			while (rs.next()) {
				newPlayer(UUID.fromString(rs.getString("uuid")));
			}
			s.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean loadNPCList() {

		TownyEcoLogger.log("Loading NPC List", DATABASE);
		if (!getContext())
			return false;
		try {
			Statement s = cntx.createStatement();
			ResultSet rs = s.executeQuery("SELECT uuid FROM " + tb_prefix + "NPCS");

			while (rs.next()) {
				newNPC(UUID.fromString(rs.getString("uuid")));
			}
			s.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean loadPlayer(TEPlayer player) {
		if (!getContext())
			return false;
		try {
			Statement s = cntx.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM " + tb_prefix + "PLAYERS " + " WHERE uuid='" + player.getUuid() + "'");

			while (rs.next()) {
				try {
					player.setLastOnline(rs.getLong("lastonline"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					player.setRegistered(rs.getLong("registered"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					player.setBalance(rs.getLong("balance"));
				} catch (Exception e) {
					e.printStackTrace();
				}


				s.close();
				return true;
			}
			return false;
		} catch (SQLException e) {
			TownyEcoLogger.log("SQLError:" + e.getMessage(), TownyEcoLogger.LogType.DATABASE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean loadNPC(TENpc npc) {
		if (!getContext())
			return false;
		try {
			Statement s = cntx.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM " + tb_prefix + "NPCS " + " WHERE uuid='" + npc.getUuid() + "'");

			while (rs.next()) {
				try {
					npc.setName(rs.getString("name"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					npc.setRegistered(rs.getLong("registered"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					npc.setBalance(rs.getLong("balance"));
				} catch (Exception e) {
					e.printStackTrace();
				}

				s.close();
				return true;
			}
			return false;
		} catch (SQLException e) {
			TownyEcoLogger.log("SQLError:" + e.getMessage(), TownyEcoLogger.LogType.DATABASE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public synchronized boolean savePlayer(TEPlayer player) {
		try {
			HashMap<String, Object> player_hm = new HashMap<>();
			player_hm.put("registered", player.getRegistered());
			player_hm.put("lastonline", player.getLastOnline());
			player_hm.put("balance", player.getBalance());

			UpdateDB("PLAYERS", player_hm, Collections.singletonList("uuid"));
			return true;

		} catch (Exception e) {
			TownyEcoLogger.log("Error while trying to saving player" + player.getUuid() + ":" + e.getMessage(), DATABASE);
		}
		return false;
	}

	@Override
	public synchronized boolean saveNPC(TENpc npc) {
		try {
			HashMap<String, Object> npc_hm = new HashMap<>();

			npc_hm.put("name", npc.getName());
			npc_hm.put("registered", npc.getRegistered());
			npc_hm.put("balance", npc.getBalance());

			UpdateDB("NPCS", npc_hm, Collections.singletonList("uuid"));
			return true;

		} catch (Exception e) {
			TownyEcoLogger.log("Error while trying to saving NPC" + npc.getUuid() + ":" + e.getMessage(), DATABASE);
		}
		return false;
	}

	@Override
	public void deletePlayer(TEPlayer player) {

		HashMap<String, Object> player_hm = new HashMap<>();
		player_hm.put("uuid", player.getUuid());
		DeleteDB("PLAYERS", player_hm);
	}

	@Override
	public void deleteNPC(TENpc npc) {

		HashMap<String, Object> npc_hm = new HashMap<>();
		npc_hm.put("uuid", npc.getUuid());
		DeleteDB("NPCS", npc_hm);
	}

	@Override
	public boolean savePlayerList() {

		return true; // We read the tables.
	}

	@Override
	public boolean saveNPCList() {

		return true; // We read the tables.
	}

}
