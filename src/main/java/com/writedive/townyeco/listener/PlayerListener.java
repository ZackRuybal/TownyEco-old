package com.writedive.townyeco.listener;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.messaging.TownyEcoLogger;
import com.writedive.townyeco.objects.TEPlayer;
import com.writedive.townyeco.tasks.PlayerLoginTask;
import com.writedive.townyeco.utilities.BukkitTools;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import static com.writedive.townyeco.messaging.TownyEcoLogger.LogType.STARTUP;

public class PlayerListener implements Listener {
	private TownyEco plugin;

	public PlayerListener(TownyEco plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (!player.isOnline()) {
			return;
		}

		// Test and kick any players with invalid names.
		if ((player.getName().trim() == null) || (player.getName().contains(" "))) {
			player.kickPlayer("Invalid name!");
			return;
		}

		if (BukkitTools.scheduleSyncDelayedTask(new PlayerLoginTask(player), 0L) == -1) {
			TownyEcoLogger.log("Loading Error: Player could not be loaded on login", STARTUP);
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		TEPlayer tePlayer = TownyEco.getDataSource().getPlayer(player.getUniqueId());
		tePlayer.setLastOnline(System.currentTimeMillis());
		TownyEco.getDataSource().savePlayer(tePlayer);
	}
}
