package com.writedive.townyeco.commands;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.objects.TEPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MoneyCommand implements CommandExecutor {
	private TownyEco plugin;

	public MoneyCommand(TownyEco plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			TEPlayer player = TownyEco.getDataSource().getPlayer(((Player) sender).getUniqueId());
			Bukkit.broadcastMessage(Double.toString(player.getBalance()));
			player.addToBalance(10.00);
			Bukkit.broadcastMessage(Double.toString(player.getBalance()));
			return true;
		} else {
			return true;
		}
	}
}
