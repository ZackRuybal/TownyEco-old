package com.writedive.townyeco.objects;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.objects.objectinterfaces.AccountHolder;

import java.util.UUID;

public class TENpc implements AccountHolder {
	private UUID uuid;
	private String name;
	private long registered;
	private double balance;

	public TENpc(UUID uuid) {
		this.uuid = uuid;
	}

	public UUID getUuid() {
		return uuid;
	}

	public long getRegistered() {
		return registered;
	}

	public void setRegistered(long registered) {
		this.registered = registered;
		TownyEco.getDataSource().saveNPC(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		TownyEco.getDataSource().saveNPC(this);
	}

	public void setBalance(double balance) {
		this.balance = balance;
		TownyEco.getDataSource().saveNPC(this);
	}

	public double getBalance() {
		return balance;
	}

	public void addToBalance(double addition) {
		this.balance = balance + addition;
		TownyEco.getDataSource().saveNPC(this);
	}

	public void removeFromBalance(double subtraction) {
		this.balance = balance - subtraction;
		TownyEco.getDataSource().saveNPC(this);
	}
}
