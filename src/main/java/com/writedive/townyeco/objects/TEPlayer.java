package com.writedive.townyeco.objects;

import com.writedive.townyeco.TownyEco;
import com.writedive.townyeco.objects.objectinterfaces.AccountHolder;

import java.util.UUID;

public class TEPlayer implements AccountHolder {
	private UUID uuid;
	private long registered, lastOnline;
	private double balance;

	public TEPlayer(UUID uuid) {
		this.uuid = uuid;
	}

	public UUID getUuid() {
		return uuid;
	}

	public long getRegistered() {
		return registered;
	}

	public void setRegistered(long registered) {
		this.registered = registered;
		TownyEco.getDataSource().savePlayer(this);
	}

	public long getLastOnline() {
		return lastOnline;
	}

	public void setLastOnline(long lastOnline) {
		this.lastOnline = lastOnline;
	}

	public void setBalance(double balance) {
		this.balance = balance;
		TownyEco.getDataSource().savePlayer(this);
	}

	public double getBalance() {
		return balance;
	}

	public void addToBalance(double addition) {
		this.balance = balance + addition;
		TownyEco.getDataSource().savePlayer(this);
	}

	public void removeFromBalance(double subtraction) {
		this.balance = balance - subtraction;
		TownyEco.getDataSource().savePlayer(this);
	}
}
