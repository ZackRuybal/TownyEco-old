package com.writedive.townyeco.objects.objectinterfaces;

public interface AccountHolder {

	public void setBalance(double balance);

	public double getBalance();

	public void addToBalance(double addition);

	public void removeFromBalance(double subtraction);
}
