package com.writedive.townyeco.configuration;

import com.writedive.util.FileMgmt;
import com.writedive.util.TimeTools;
import org.bukkit.Bukkit;

import java.io.File;

public class ConfigurationHandler {
	private static CommentedConfiguration config, newConfig;

	public static void loadConfig(String filepath, String version) {

		File file = FileMgmt.CheckYMLExists(new File(filepath));
		if (file != null) {

			// read the config.yml into memory
			config = new CommentedConfiguration(file);
			if (!config.load()) {
				System.out.print("Failed to load Config!");
			}

			setDefaults(version, file);

			config.save();
		}
	}

	/**
	 * Builds a new config reading old config data.
	 */
	private static void setDefaults(String version, File file) {

		newConfig = new CommentedConfiguration(file);
		newConfig.load();

		for (ConfigNodes root : ConfigNodes.values()) {
			if (root.getComments().length > 0) {
				addComment(root.getRoot(), root.getComments());
			}
			if (root.getRoot().equals(ConfigNodes.VERSION.getRoot())) {
				setNewProperty(root.getRoot(), version);
			} else if (root.getRoot().equals(ConfigNodes.LAST_RUN_VERSION.getRoot())) {
				setNewProperty(root.getRoot(), getLastRunVersion(version));
			} else
				setNewProperty(root.getRoot(), (config.get(root.getRoot().toLowerCase()) != null) ? config.get(root.getRoot().toLowerCase()) : root.getDefault());

		}

		config = newConfig;
		newConfig = null;
	}

	private static void addComment(String root, String... comments) {

		newConfig.addComment(root.toLowerCase(), comments);
	}

	private static void setNewProperty(String root, Object value) {

		if (value == null) {
			// System.out.print("value is null for " + root.toLowerCase());
			value = "";
		}
		newConfig.set(root.toLowerCase(), value.toString());
	}

	private static String getLastRunVersion(String currentVersion) {

		return getString(ConfigNodes.LAST_RUN_VERSION.getRoot(), currentVersion);
	}

	private static String getString(String root, String def) {

		String data = config.getString(root.toLowerCase(), def);
		if (data == null) {
			sendError(root.toLowerCase() + " from config.yml");
			return "";
		}
		return data;
	}

	private static void sendError(String msg) {

		Bukkit.getLogger().info("Error could not read " + msg);
	}


	public static String getString(ConfigNodes node) {

		return config.getString(node.getRoot().toLowerCase(), node.getDefault());

	}

	public static boolean getBoolean(ConfigNodes node) {

		return Boolean.parseBoolean(config.getString(node.getRoot().toLowerCase(), node.getDefault()));
	}

	public static double getDouble(ConfigNodes node) {

		try {
			return Double.parseDouble(config.getString(node.getRoot().toLowerCase(), node.getDefault()).trim());
		} catch (NumberFormatException e) {
			sendError(node.getRoot().toLowerCase() + " from config.yml");
			return 0.0;
		}
	}

	public static int getInt(ConfigNodes node) {

		try {
			return Integer.parseInt(config.getString(node.getRoot().toLowerCase(), node.getDefault()).trim());
		} catch (NumberFormatException e) {
			sendError(node.getRoot().toLowerCase() + " from config.yml");
			return 0;
		}
	}

	public static String getLoadDatabase() {

		return getString(ConfigNodes.PLUGIN_DATABASE_LOAD);
	}

	public static String getSaveDatabase() {

		return getString(ConfigNodes.PLUGIN_DATABASE_SAVE);
	}

	// SQL
	public static String getSQLHostName() {

		return getString(ConfigNodes.PLUGIN_DATABASE_HOSTNAME);
	}

	public static String getSQLPort() {

		return getString(ConfigNodes.PLUGIN_DATABASE_PORT);
	}

	public static String getSQLDBName() {

		return getString(ConfigNodes.PLUGIN_DATABASE_DBNAME);
	}

	public static String getSQLTablePrefix() {

		return getString(ConfigNodes.PLUGIN_DATABASE_TABLEPREFIX);
	}

	public static String getSQLUsername() {

		return getString(ConfigNodes.PLUGIN_DATABASE_USERNAME);
	}

	public static String getSQLPassword() {

		return getString(ConfigNodes.PLUGIN_DATABASE_PASSWORD);
	}

	public static boolean getSQLUsingSSL() {

		return getBoolean(ConfigNodes.PLUGIN_DATABASE_SSL);
	}

	public static long getBackupLifeLength() {

		long t = TimeTools.getMillis(getString(ConfigNodes.PLUGIN_BACKUPS_ARE_DELETED_AFTER));
		long minT = TimeTools.getMillis("1d");
		if (t >= 0 && t < minT)
			t = minT;
		return t;
	}

	public static String getBackupType() {

		return getString(ConfigNodes.PLUGIN_BACKUP_TYPE);
	}

	public static String getCurrencyNameSingular() {
		return getString(ConfigNodes.ECONOMY_NAME_SINGULAR);
	}

	public static String getCurrencyNamePlural() {
		return getString(ConfigNodes.ECONOMY_NAME_PLURAL);
	}
}
