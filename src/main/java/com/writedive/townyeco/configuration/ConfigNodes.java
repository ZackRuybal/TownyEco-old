package com.writedive.townyeco.configuration;

public enum ConfigNodes {
	VERSION_HEADER("version", ""),
	VERSION(
			"version.version",
			"",
			"# This is the current version of Towny.  Please do not edit."),
	LAST_RUN_VERSION(
			"version.last_run_version",
			"",
			"# This is for showing the change-log on updates.  Please do not edit."),
	PLUGIN(
			"database",
			"",
			"############################################################",
			"# +------------------------------------------------------+ #",
			"# |                Database-Configuration                | #",
			"# +------------------------------------------------------+ #",
			"############################################################"),

	PLUGIN_DATABASE_LOAD("database.database_load", "flatfile", "# Valid load and save types are: flatfile, mysql, h2 or sqlite."),
	PLUGIN_DATABASE_SAVE("database.database_save", "flatfile"),

	PLUGIN_DATABASE_SQL_HEADER(
			"database.sql",
			"",
			"# SQL database connection details (IF set to use SQL)."),
	PLUGIN_DATABASE_HOSTNAME("database.sql.hostname", "localhost"),
	PLUGIN_DATABASE_PORT("database.sql.port", "3306"),
	PLUGIN_DATABASE_DBNAME("database.sql.dbname", "towny"),
	PLUGIN_DATABASE_TABLEPREFIX("database.sql.table_prefix", "townyeco_"),
	PLUGIN_DATABASE_USERNAME("database.sql.username", "root"),
	PLUGIN_DATABASE_PASSWORD("database.sql.password", ""),
	PLUGIN_DATABASE_SSL("database.sql.ssl", "false"),

	PLUGIN_DAILY_BACKUPS_HEADER(
			"database.daily_backups",
			"",
			"# Backup settings."),
	PLUGIN_DAILY_BACKUPS("database.daily_backups", "true"),
	PLUGIN_BACKUPS_ARE_DELETED_AFTER(
			"database.backups_are_deleted_after",
			"90d"),
	PLUGIN_BACKUP_TYPE(
			"database.backup_type",
			"zip",
			"# Valid entries are: zip, none."),
	ECONOMY(
			"economy",
			"",
			"############################################################",
			"# +------------------------------------------------------+ #",
			"# |                   Economy-Settings                   | #",
			"# +------------------------------------------------------+ #",
			"############################################################"
	),
	ECONOMY_NAME_SINGULAR(
			"economy.name_singular",
			"Dollar",
			"# The singular name of the main currency"
	),
	ECONOMY_NAME_PLURAL(
			"economy.name_plural",
			"Dollars",
			"# The plural name of the main currency."
	);

	private final String Root;
	private final String Default;
	private String[] comments;

	ConfigNodes(String root, String def, String... comments) {

		this.Root = root;
		this.Default = def;
		this.comments = comments;
	}

	/**
	 * Retrieves the root for a config option
	 *
	 * @return The root for a config option
	 */
	public String getRoot() {

		return Root;
	}

	/**
	 * Retrieves the default value for a config path
	 *
	 * @return The default value for a config path
	 */
	public String getDefault() {

		return Default;
	}

	/**
	 * Retrieves the comment for a config path
	 *
	 * @return The comments for a config path
	 */
	public String[] getComments() {

		if (comments != null) {
			return comments;
		}

		String[] comments = new String[1];
		comments[0] = "";
		return comments;
	}
}
