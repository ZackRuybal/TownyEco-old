# Towny Eco
A complete rewrite of HyperConomy more suitable for closed-economies using Towny

## Development Builds [![Build Status](https://ci.auranode.com/job/TownyEco/badge/icon)](https://ci.auranode.com/job/TownyEco/)
Dev builds can be found on our CI at [ci.auranode.com](https://ci.auranode.com/job/TownyEco)